TYPE=VIEW
query=select `prs`.`student`.`Ime_Prezime` AS `Ime_Prezime`,`prs`.`fakultet`.`Ime_Fakulteta` AS `Fakultet`,`prs`.`zemlja`.`Ino_Fakultet` AS `Ino_Fakultet`,`prs`.`zemlja`.`Mesto` AS `Mesto` from (((`prs`.`prs` join `prs`.`student` on(`prs`.`student`.`ID_Studenta` = `prs`.`prs`.`StudentID`)) join `prs`.`fakultet` on(`prs`.`fakultet`.`ID_Fakulteta` = `prs`.`prs`.`FakultetID`)) join `prs`.`zemlja` on(`prs`.`zemlja`.`ID_Boravista` = `prs`.`prs`.`BoravisteID`))
md5=4f9aadf39923e5bf01fc4b939c13d9ff
updatable=1
algorithm=0
definer_user=root
definer_host=localhost
suid=2
with_check_option=0
timestamp=2019-12-09 20:52:43
create-version=2
source=SELECT\nIme_Prezime,\nIme_Fakulteta AS \'Fakultet\',\nIno_Fakultet,\nMesto\nFROM\nPRS\nINNER JOIN Student ON Student.ID_Studenta = StudentID\nINNER JOIN Fakultet ON Fakultet.ID_Fakulteta = PRS.FakultetID\nINNER JOIN Zemlja ON Zemlja.ID_Boravista = BoravisteID
client_cs_name=cp850
connection_cl_name=cp850_general_ci
view_body_utf8=select `prs`.`student`.`Ime_Prezime` AS `Ime_Prezime`,`prs`.`fakultet`.`Ime_Fakulteta` AS `Fakultet`,`prs`.`zemlja`.`Ino_Fakultet` AS `Ino_Fakultet`,`prs`.`zemlja`.`Mesto` AS `Mesto` from (((`prs`.`prs` join `prs`.`student` on(`prs`.`student`.`ID_Studenta` = `prs`.`prs`.`StudentID`)) join `prs`.`fakultet` on(`prs`.`fakultet`.`ID_Fakulteta` = `prs`.`prs`.`FakultetID`)) join `prs`.`zemlja` on(`prs`.`zemlja`.`ID_Boravista` = `prs`.`prs`.`BoravisteID`))
mariadb-version=100500
